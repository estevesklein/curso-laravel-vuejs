# Sistema de gestão Administrativa / Financeira

Desenvolvimento de uma plataforma SAAS de um sistema de gestão Administrativa / Financeira utilizando Laravel e VueJs.

### Instalação

Instalar as dependências globais

```sh
$ npm install -g webpack-dev-server
$ npm install -g babel
$ npm install -g node-sass
$ npm install -g webpack
```

Instalar as dependências do projeto

```sh
$ npm install --no-bin-link
```

### Ambiente de desenvolvimento

```sh
$ npm run start
```

http://192.168.10.10:8080

* run API
cd /vue-bills-api
php -S 0.0.0.0:8000

* Tests in Postman
http://192.168.10.10:8000


### Ambiente de desenvolvimento

Gerar o build do projeto

```sh
$ npm run build
```