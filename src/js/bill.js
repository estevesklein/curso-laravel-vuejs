export class BillPay{

	constructor(data = {}){

		//console.log('contructor BillPay');

		this.date_due = '';
		this.name = '';
		this.value = 0;
		this.done = false;
		
		Object.assign(this,data);
	}

	toJSON(){

		//console.log(this.date_due);

		return {
			date_due: this.date_due.toISOString().substring(0,10),
			name: this.name,
			value: this.value,
			done: this.done
		}
	}
}